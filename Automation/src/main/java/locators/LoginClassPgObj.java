/**
 * 
 */
package locators;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

/**
 * @author r.singh.tomar
 *
 */
public class LoginClassPgObj {

	WebDriver driver;
	public LoginClassPgObj(WebDriver ldriver) {
	this.driver = ldriver;
		// TODO Auto-generated constructor stub
	}
	@FindBy(xpath = "//input[@class='_2zrpKA _1dBPDZ']")
	WebElement usernameTextBox;
	
	@FindBy(xpath = "//input[@class='_2zrpKA _3v41xv _1dBPDZ']")
	WebElement passwordTextBox;
	
	@FindBy(xpath="//button[@class='_2AkmmA _1LctnI _7UHT_c']")
	WebElement loginButton;
	
	@FindBy(xpath = "//div[@class='_2aUbKa' and text()='Rahul Singh']")
	WebElement profileICon;
	
	public boolean loginInToFlipkart(String username,String password) {
		try {
			usernameTextBox.sendKeys(username);
			passwordTextBox.sendKeys(password);
			loginButton.click();
			return true;
		}catch(Exception e) {
			System.out.println(e);
			return false;
		}
	}
	public void verifyProfileIcon() {
		Assert.assertTrue(profileICon.isDisplayed());
	}

}
