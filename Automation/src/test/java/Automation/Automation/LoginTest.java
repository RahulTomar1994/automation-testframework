/**
 * 
 */
package Automation.Automation;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import Automation.Automation.BaseClass;
import locators.LoginClassPgObj;

/**
 * @author r.singh.tomar
 *
 */
public class LoginTest extends BaseClass{
	public static final Logger LOGGER = LogManager.getLogger(LoginTest.class);
	@Test(priority=0)
	public void verifyLogin() {
		
		LoginClassPgObj loginClassPgObj = PageFactory.initElements(driver, LoginClassPgObj.class);
		driver.get(prop.getProperty("url"));
		LOGGER.info("Url Is :"+prop.getProperty("url"));
		loginClassPgObj.loginInToFlipkart(prop.getProperty("userName"), prop.getProperty("passWord"));
	}
	@Test(priority=1,enabled=true)
	public void verifyHomePage() throws InterruptedException {
		
		LoginClassPgObj loginClassPgObj = PageFactory.initElements(driver, LoginClassPgObj.class);
		driver.get(prop.getProperty("url"));
		loginClassPgObj.loginInToFlipkart(prop.getProperty("userName"), prop.getProperty("passWord"));
		Thread.sleep(2000);
		loginClassPgObj.verifyProfileIcon();
		
	}

}
